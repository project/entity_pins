<?php

namespace Drupal\entity_pins\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Pin type entities.
 */
interface PinTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
