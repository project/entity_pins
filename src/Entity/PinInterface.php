<?php

namespace Drupal\entity_pins\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Pin entities.
 *
 * @ingroup entity_pins
 */
interface PinInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Pin name.
   *
   * @return string
   *   Name of the Pin.
   */
  public function getName();

  /**
   * Sets the Pin name.
   *
   * @param string $name
   *   The Pin name.
   *
   * @return \Drupal\entity_pins\Entity\PinInterface
   *   The called Pin entity.
   */
  public function setName($name);

  /**
   * Gets the Pin creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Pin.
   */
  public function getCreatedTime();

  /**
   * Sets the Pin creation timestamp.
   *
   * @param int $timestamp
   *   The Pin creation timestamp.
   *
   * @return \Drupal\entity_pins\Entity\PinInterface
   *   The called Pin entity.
   */
  public function setCreatedTime($timestamp);

}
