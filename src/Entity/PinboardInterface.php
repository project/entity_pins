<?php

namespace Drupal\entity_pins\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Pinboard entities.
 *
 * @ingroup entity_pins
 */
interface PinboardInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Pinboard name.
   *
   * @return string
   *   Name of the Pinboard.
   */
  public function getName();

  /**
   * Sets the Pinboard name.
   *
   * @param string $name
   *   The Pinboard name.
   *
   * @return \Drupal\entity_pins\Entity\PinboardInterface
   *   The called Pinboard entity.
   */
  public function setName($name);

  /**
   * Gets the Pinboard creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Pinboard.
   */
  public function getCreatedTime();

  /**
   * Sets the Pinboard creation timestamp.
   *
   * @param int $timestamp
   *   The Pinboard creation timestamp.
   *
   * @return \Drupal\entity_pins\Entity\PinboardInterface
   *   The called Pinboard entity.
   */
  public function setCreatedTime($timestamp);

}
