<?php

namespace Drupal\entity_pins\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Pin type entity.
 *
 * @ConfigEntityType(
 *   id = "pin_type",
 *   label = @Translation("Pin type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_pins\PinTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_pins\Form\PinTypeForm",
 *       "edit" = "Drupal\entity_pins\Form\PinTypeForm",
 *       "delete" = "Drupal\entity_pins\Form\PinTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "pin_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "pin",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pin_type/{pin_type}",
 *     "add-form" = "/admin/structure/pin_type/add",
 *     "edit-form" = "/admin/structure/pin_type/{pin_type}/edit",
 *     "delete-form" = "/admin/structure/pin_type/{pin_type}/delete",
 *     "collection" = "/admin/structure/pin_type"
 *   }
 * )
 */
class PinType extends ConfigEntityBundleBase implements PinTypeInterface {

  /**
   * The Pin type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Pin type label.
   *
   * @var string
   */
  protected $label;

}
