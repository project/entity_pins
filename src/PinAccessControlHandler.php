<?php

namespace Drupal\entity_pins;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Pin entity.
 *
 * @see \Drupal\entity_pins\Entity\Pin.
 */
class PinAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_pins\Entity\PinInterface $entity */
    $isOwner = $entity->getOwnerId() === $account->id();
    
    if ($account->hasPermission('administer pins')) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }
    
    $allowAccess = FALSE;
    switch ($operation) {
      case 'view':
        $allowAccess = $account->hasPermission('view any pins') || ($isOwner && $account->hasPermission('view own pins'));
        break;

      case 'update':
        $allowAccess = $account->hasPermission('edit any pins') || ($isOwner && $account->hasPermission('edit own pins'));
        break;

      case 'delete':
        $allowAccess = $account->hasPermission('delete any pins') || ($isOwner && $account->hasPermission('delete own pins'));
        break;
    }
    
    if ($allowAccess) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add pins');
  }

}
