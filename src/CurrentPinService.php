<?php

namespace Drupal\entity_pins;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * CurrentPinService service.
 */
class CurrentPinService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The Pin storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $pinStorage;

  /**
   * Constructs a currentpinservice object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $account) {
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->pinStorage = $this->entityTypeManager->getStorage('pin');
  }

  /**
   * Load Pin if exist.
   *
   * Try to load current entity Pin if exist or return NULL.
   *
   * @param int|string $entityId
   *   Entity ID.
   *
   * @return mixed|null
   *   Pin or NULL.
   */
  public function loadPin($entityId) {
    $pins = $this->pinStorage->loadByProperties([
      'type' => 'content_pin',
      'field_content' => $entityId,
      'user_id' => $this->account->id(),
    ]);

    $pin = NULL;
    if (!empty($pins)) {
      $pin = reset($pins);
    }

    return $pin;
  }

  /**
   * Get current Pin Pinboards.
   *
   * @param int|string $entity_id
   *   Entity ID.
   *
   * @return array
   *   Array of Pinboards IDs.
   */
  public function getPinPinboards($entity_id) {
    $pin = $this->loadPin($entity_id);

    $pinPinboards = [];

    if (!is_null($pin)) {
      $fieldPinboards = $pin->get('field_pinboards')->getValue();
      $pinPinboards = array_column($fieldPinboards, 'target_id');
    }

    return $pinPinboards;
  }

}
