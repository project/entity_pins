<?php

namespace Drupal\entity_pins\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Class PinTypeForm.
 */
class PinTypeForm extends EntityForm {

  use MessengerTrait;
  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $pin_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $pin_type->label(),
      '#description' => $this->t("Label for the Pin type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $pin_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\entity_pins\Entity\PinType::load',
      ],
      '#disabled' => !$pin_type->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $pin_type = $this->entity;
    $status = $pin_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Pin type.', [
          '%label' => $pin_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Pin type.', [
          '%label' => $pin_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($pin_type->toUrl('collection'));
  }

}
