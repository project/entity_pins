<?php

namespace Drupal\entity_pins\Form;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_pins\CurrentPinService;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * ModalForm class.
 */
class ModalForm extends FormBase {
  
  use TranslationTrait;
  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;
  
  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\entity_pins\CurrentPinService
   */
  private $currentPinService;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $pinStorage;
  
  /**
   * Constructs a new AjaxAddToCartForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountInterface $currentUser, CurrentPinService $currentPinService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->currentPinService = $currentPinService;
    $this->pinStorage = $this->entityTypeManager->getStorage('pin');
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('entity_pins.current_pin')
    );
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pin_content_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $entity = NULL) {
    $form['entity'] = [
      '#type' => 'value',
      '#value' => $entity->id(),
    ];
    
    $form['entity_type'] = [
      '#type' => 'value',
      '#value' => $entity->getEntityTypeId(),
    ];
    
    $form['inner'] = [
      '#type' => 'container',
      '#attributes' => array('id' => 'pin_content_form_modal'),
    ];
    
    $pinboards = $this->entityTypeManager->getStorage('pinboard')
      ->loadByProperties([
        'user_id' => $this->currentUser->id()
      ]);
    
    if (!empty($pinboards)) {
      $form['inner']['pinboards'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('My pinboards'),
      ];
      $form['inner']['pinboards']['pinboards'] = [
        '#tree' => TRUE,
      ];
      foreach ($pinboards as $pinboard) {
        $form['inner']['pinboards']['pinboards'][$pinboard->id()] = [
          '#type' => 'checkbox',
          '#title' => $pinboard->getName(),
        ];
      }

      // If current Pin exist load Pin Pinboards.
      $currentEntityPinPinboards = $this->currentPinService->getPinPinboards($entity->id());

      if (!empty($currentEntityPinPinboards)) {
        // Set Pin current Pinboards to "checked".
        foreach ($currentEntityPinPinboards as $item) {
          if (isset($form['inner']['pinboards']['pinboards'][$item])) {
            $form['inner']['pinboards']['pinboards'][$item]['#default_value'] = TRUE;
          }
        }
      }
  
      $form['inner']['pinboards']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#attributes' => [
          'class' => ['use-ajax'],
        ],
        '#ajax' => [
          'callback' => [$this, 'addToPinboard'],
          'event' => 'click',
        ],
      ];
    }
    
    $form['inner']['new_pinboard']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pinboard name'),
    ];
  
    $form['inner']['new_pinboard']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create pinboard'),
      '#attributes' => [
        'class' => ['use-ajax'],
      ],
      '#ajax' => [
        'callback' => [$this, 'createPinboard'],
        'event' => 'click',
        'wrapper' => 'pin_content_form_modal',
      ],
    ];
    $form_state->setCached(FALSE);
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    
    return $form;
  }
  
  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function addToPinboard(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#submit-error-form', $form));
    }
    else {
      $formValues = $form_state->getValues();
      // Load Pin.
      $pin = $this->currentPinService->loadPin($formValues['entity']);
      // Filter out pinboards.
      $selectedPinboards = array_keys(array_filter($formValues['pinboards']));

      $pinLinkText = '';

      // If no pinboards are selected and pin exists, then delete it.
      if (!is_null($pin) && empty($selectedPinboards)) {
        $pinLinkText = 'Pin Content';
        $pin->delete();
      }
      // If pins are selected and pin doesn't exist, create new pin and assign it to selected pinboards.
      elseif (is_null($pin) && !empty($selectedPinboards)) {
        $this->pinStorage->create([
          'type' => 'content_pin',
          'field_content' => $formValues['entity'],
          'field_pinboards' => $selectedPinboards,
        ])->save();
        $pinLinkText = 'Edit Pin';
      }
      // If pins are selected and pin does exist, assign it to selected pinboards.
      elseif (!is_null($pin) && !empty($selectedPinboards)) {
        $pin->set('field_pinboards', $selectedPinboards);
        $pin->save();
        $pinLinkText = 'Edit Pin';
      }
  
      $response->addCommand(
        new OpenModalDialogCommand($this->t('Pin a content'), $this->t('Content was added to selected pinboard.'), ['width' => '80%'])
      );
      $response->addCommand(new HtmlCommand('#pin-link', $pinLinkText));
    }
    return $response;
  }
  
  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function createPinboard(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#submit-error-form', $form));
    }
    else {
      $formValues = $form_state->getValues();
      $newPinboard = $this->entityTypeManager->getStorage('pinboard')
        ->create([
          'name' => $formValues['name'],
        ]);
      $newPinboard->save();
      $form['inner']['pinboards']['pinboards'][$newPinboard->id()] = [
        '#type' => 'checkbox',
        '#title' => $newPinboard->getName(),
        '#name' => "pinboards[{$newPinboard->id()}]",
      ];
      $form['inner']['new_pinboard']['name']['#value'] = '';
      $response->addCommand(new ReplaceCommand('#pin_content_form_modal', $form));
    }
    return $response;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity_pins.error_thanks');
  }
  
}