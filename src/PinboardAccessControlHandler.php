<?php

namespace Drupal\entity_pins;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Pinboard entity.
 *
 * @see \Drupal\entity_pins\Entity\Pinboard.
 */
class PinboardAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_pins\Entity\PinboardInterface $entity */
    $isOwner = $entity->getOwnerId() === $account->id();
  
    if ($account->hasPermission('administer pinboards')) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }
  
    $allowAccess = FALSE;
    switch ($operation) {
      case 'view':
        $allowAccess = $account->hasPermission('view any pinboards') || ($isOwner && $account->hasPermission('view own pinboards'));
        break;
    
      case 'update':
        $allowAccess = $account->hasPermission('edit any pinboards') || ($isOwner && $account->hasPermission('edit own pinboards'));
        break;
    
      case 'delete':
        $allowAccess = $account->hasPermission('delete any pinboards') || ($isOwner && $account->hasPermission('delete own pinboards'));
        break;
    }
  
    if ($allowAccess) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add pinboards');
  }

}
